This repository contains some useful parts for the mini-DebConf organized in May 2019 in Marseille (see https://minidebconf-mrs.debian.net/).

The **slides** directory contains a reveal-js project for the slideshow that will be displayed to welcome attendees and to give information throughout the days.
